
# Conservation of Optical Chirality in Superconductors as a measure for $5$ dimensional Electromagnetism

## Abstract

Currently only three spatial and one temporal dimensions are considered to be ``physical''. Recently, 
solutions to a plethora of questions have used the notion of extra-dimensions. The experimental 
verification of the existence of such extra dimensions, however, remains elusive. Here by applying 
standard electromagnetic theory to five dimensional Minkowski space-time, we propose a novel test 
ground to search for a new non-compactified lengthlike extra dimension. In this regard, we propose 
superconductors as a test bed for this hypothetical new extra dimension.

## Conclusion

In summary, we extended Maxwell's equations to $5$-Minkowski space-time, and showed that London 
and Josephson equations of superconductors can be ``fitted'' into them. Following this method we 
proposed a new experiment to test the existence of a new lengthlike dimension. We showed that 
conservation or non-conservation of optical chirality inside a Josephson barrier under specific 
experimental conditions may act as a measure for the existence of an extra lengthlike dimension. 
Also we described under what conditions such an additional dimension avoids detection in the vacuum.


## This is a green open access article

As science, just like economics, needs freedom to exist, I encourage all researchers to publish their research 
in git repositories, because this technology is so suitable to keep track of the time events, which is exactly 
what science is needed to flourish.
